use rand::Rng;
use std::cmp::Ordering;
use std::io;

fn main() {
    let random_number = rand::thread_rng().gen_range(1..=100);
    println!("Tebak angka 1-100. {random_number}");

    loop {
        let mut input = String::new();

        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");

        let input: u32 = match input.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        match input.cmp(&random_number) {
            Ordering::Less => println!("Kekecilan!"),
            Ordering::Equal => {
                println!("Betul");
                break;
            }
            Ordering::Greater => {
                println!("Kegedean!");
            }
        }
        println!("Your input: {input}");
    }
}
